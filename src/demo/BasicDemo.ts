export default class BasicDemo {
    private _view: fgui.GComponent;
    private _backBtn: fgui.GObject;
    private _demoContainer: fgui.GComponent;
    private _cc: fgui.Controller;

    private _demoObjects: any;
    constructor() {
        fgui.UIPackage.loadPackage("res/UI/Basic", Laya.Handler.create(this, this.onUILoaded));
    }
    onUILoaded() {
        //* 將 Basic 顯示出來
        // 調用 Basic 的 Main（組件）
        this._view = fgui.UIPackage.createObject("Basic", "Main").asCom;
        this._view.makeFullScreen();
        // 增加為 GObject 的子節點
        fgui.GRoot.inst.addChild(this._view);


        //* 取得 btn_Back 給 _backBtn 並控制
        // 取得 Basic > Main 的 子對像 btn_Back 給 _backBtn
        this._backBtn = this._view.getChild("btn_Back");
        // _backBtn 可見性：虛
        this._backBtn.visible = false;
        // _backBtn 的 滑鼠事件（FGUI的方法），調用 BasicDemo.onClickBack()
        this._backBtn.onClick(this, this.onClickBack);


        //* 取得 Basic Main 的子對像「容器」（組件）
        this._demoContainer = this._view.getChild("container").asCom;

        //* 取得 Main 的 控制器到 C1
        this._cc = this._view.getController("c1");

        //* 遍歷所有物件，如果屬於 group 並名稱為 btns 就綁上onClick 事件，跑runDemo
        var cnt: number = this._view.numChildren;
        //? 怎麼裝的不清楚耶
        for (var i: number = 0; i < cnt; i++) {
            var obj: fgui.GObject = this._view.getChildAt(i);
            console.warn('正在裝什麼==== ', obj);
            // * 判斷 group 如果是空的，並且group 名字為 == btns 就綁上滑鼠事件
            if (obj.group != null && obj.group.name == "btns")
                obj.onClick(this, this.runDemo);
        }
        //* 這裡很重要，必須要給 _demoObjects 一個空物件，後面大家要丟type 屬性使用
        //* 他是為了被其他 private 調用的
        this._demoObjects = {};

    }

    destroy() {
        fgui.UIPackage.removePackage("Basic");

    }
    private runDemo(evt: Laya.Event): void {
        // 取得字串 例：btn_Button ，只會取到 type = Button 下方給 switch 使用
        var type: string = fgui.GObject.cast(evt.currentTarget).name.substr(4);
        console.warn('type= ', type);
        console.warn('UIPackage= ', fgui.UIPackage.createObject("Basic", "Demo_" + type).asCom);
        //* 這時候的 obj 會被指定一個暫時的 {} 空物件，這時的_demoObjects 就不會是undifine
        var obj: fgui.GComponent = this._demoObjects[type];
        console.warn('打印this_demoObjects[指定誰]', obj);
        // 把 Button 的組件 倒到 obj
        if (obj == null) {
            obj = fgui.UIPackage.createObject("Basic", "Demo_" + type).asCom;
            // 再把 obj 再倒回給 _demoObjects 目前他是 物件導向
            this._demoObjects[type] = obj;
            console.warn('裝裝裝= ', obj);
        }
        // 刪除 container 下的子物件
        this._demoContainer.removeChildren();
        // 增加 obj 給 container
        this._demoContainer.addChild(obj);
        // 控制器轉到 1，將demoContainer 顯示出來
        this._cc.selectedIndex = 1;
        // 將_backBtn 顯示出來
        this._backBtn.visible = true;

        //* 判斷有沒有其他要調用的功能
        switch (type) {
            case "Text":
                this.playText();
                break;
        }
    }



    //* text 控制文字的行為方法，在這裡
    private playText(): void {
        var obj: fgui.GComponent = this._demoObjects["Text"];
        //* n12 是富文本，可以「點擊」、「觸摸事件」和「帶網址」
        //! obj.getChild("n12").on(Laya.Event.LINK, this, this.__clickLink);
        //* Get Input 按鈕行為，會把n22的內容，取到n24裡
        obj.getChild("InputButton").onClick(this, this.__clickGetInput);
    }
    //* 文字按鈕被點擊後，就會把n24 的內容 給n22去
    private __clickGetInput(): void {
        var obj: fgui.GComponent = this._demoObjects["Text"];
        obj.getChild("OutputText").text = obj.getChild("InputText").text;
    }



    //* btn_Back 按下時，把控制器調回零，並把 btn_Back 隱藏起來
    private onClickBack(evt: Event): void {
        // 將 _cc.selectedIndex 渲染順序設為 0
        this._cc.selectedIndex = 0;
        // _backBtn 可見性 虛（隱藏）
        this._backBtn.visible = false;
    }
}